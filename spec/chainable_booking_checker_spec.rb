# frozen_string_literal: true

$LOAD_PATH.push './src'

require 'chainable_booking'

module ChainableBooking
  describe 'module methods' do
    include ChainableBooking

    describe '#car_next_door' do
      it 'is true when van, sydney, and 1 hour given' do
        expect(
          car_next_door(book('Van').at('Sydney').duration(1).hours)
        ).to be true
      end

      it 'is true when melbourne, small car, and 30 mins given' do
        expect(
          car_next_door(at('Melbourne').book('Small Car').duration(30).minutes)
        ).to be true
      end

      it 'is true when 1 week, gold coast, and jet-ski given' do
        expect(
          car_next_door(duration(1).weeks.at('Gold Coast').book('Jet-ski'))
        ).to be true
      end

      it 'is false when brisbane and submarine given (missing duration)' do
        expect(car_next_door(at('Brisbane').book('Submarine'))).to be false
      end

      it 'is false when van and 2 days given (missing location)' do
        expect(car_next_door(book('Van').duration(2).days)).to be false
      end

      it 'is false when sydney, and 1 hour given (missing car type)' do
        expect(car_next_door(at('Sydney').duration(1).hours)).to be false
      end
    end

    describe Checker do
      describe '#duration' do
        it 'results in duration' do
          expect(Checker.new.duration(0)).to be_kind_of(Duration)
        end
      end

      describe '#book' do
        it 'raises error if non-string is passed' do
          expect { Checker.new.book(nil) }.to raise_error(ArgumentError)
        end
      end

      describe '#at' do
        it 'raises error if non-string is passed' do
          expect { Checker.new.at(nil) }.to raise_error(ArgumentError)
        end
      end
    end

    describe 'Duration' do
      describe '#initialize' do
        it 'raises error if string is passed for number' do
          expect do
            Duration.new('foo', Checker.new)
          end.to raise_error(ArgumentError)
        end
      end

      describe '#minutes' do
        it 'gives back a checker' do
          expect(Checker.new.duration(1).minutes).to be_kind_of(Checker)
        end
      end

      describe '#hours' do
        it 'gives back a checker' do
          expect(Checker.new.duration(1).hours).to be_kind_of(Checker)
        end
      end

      describe '#days' do
        it 'gives back a checker' do
          expect(Checker.new.duration(1).days).to be_kind_of(Checker)
        end
      end

      describe '#weeks' do
        it 'gives back a checker' do
          expect(Checker.new.duration(1).weeks).to be_kind_of(Checker)
        end
      end
    end
  end
end

# frozen_string_literal: true

# main chainable booking module
module ChainableBooking
  def book(type)
    Checker.new.book(type)
  end

  def at(location)
    Checker.new.at(location)
  end

  def duration(num)
    Checker.new.duration(num)
  end

  def car_next_door(check)
    check.complete?
  end

  # checker is called from car_next_door method and handles actual checking
  class Checker
    def book(type)
      unless string?(type)
        raise ArgumentError, "#{type} doesn't look like a string"
      end

      @type = type
      self
    end

    def at(location)
      unless string?(location)
        raise ArgumentError, "#{location} doesn't look like a string"
      end

      @location = location
      self
    end

    def duration(num)
      @duration = Duration.new(num, self)
    end

    def complete?
      type? && location? && duration?
    end

    private

    def string?(str)
      str.to_s == str
    end

    def type?
      @type != nil
    end

    def location?
      @location != nil
    end

    def duration?
      @duration != nil
    end
  end

  # duration is represented in a class to allow only chaining to 'minutes' etc
  class Duration
    def initialize(num, checker)
      unless number?(num)
        raise ArgumentError, "#{num} doesn't seem to be a number"
      end

      @num = num
      @checker = checker
    end

    def minutes
      @checker
    end

    def hours
      @checker
    end

    def days
      @checker
    end

    def weeks
      @checker
    end

    private

    def number?(num)
      num.to_f.to_s == num.to_s || num.to_i.to_s == num.to_s
    end
  end
end

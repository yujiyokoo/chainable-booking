# Chainable Booking Module

## Usage
This is the ChainableBooking module which lets you specify location, duration and car type.

After you include this module:
```
> require './src/chainable_booking'
> include ChainableBooking
```
you will be able to use `book`, `at` and `duration` to start a booking chain:
```
> booking = at('Tokyo').book('Sedan').duration(20).hours
> incomplete_booking = at('Paris').book('Sedan')
```

The `car_next_door` method is provided by this module to check that all 3 conditions are provided:
```
> car_next_door(booking)
=> true
> car_next_door(incomplete_booking)
=> false
```

See `spec/chainable_booking_checker_spec.rb` for usage examples.

## Testing

Rspec is used for testing. To run the specs, do:
```
$ bundle install
$ bundle exec rspec
```
